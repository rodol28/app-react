import React, { Component } from 'react';
import './App.css';

import Navigation from './components/Navigation';
import TasksForm from './components/TasksForm';

import tasks from './tasks.json';


class App extends Component {

  constructor() {
    super();
    this.state = tasks;
    this.addTask = this.addTask.bind(this);
  }

  addTask(task) {
    this.setState({
      tasks: [...this.state.tasks, task]
    });
  }

  removeTasks(index) {
    if(window.confirm('Are you sure you want to delete it?')) {
      this.setState({
        tasks: this.state.tasks.filter((e, i) => {
          return i !== index;
        })
      });
    }
  }


  render() {

    const tasks = this.state.tasks.map((task, i) => {
      return (
        <div className="col-md-4" key={i}>
          <div className="card m-2">
            <div className="card-header">
              <h3>{task.title}</h3>
              <span className="badge badge-pill badge-danger ml-2">{task.priority}</span>
            </div>
            <div className="card-body">
              <p>{task.description}</p>
              <p><mark>{task.responsible}</mark></p>
            </div>
            <div className="card-footer">
              <button className="btn btn-primary" onClick={this.removeTasks.bind(this, i)}>Delete</button>
            </div>
          </div>
        </div>
      )
    });


    return (
      <div className="App">

        <Navigation title="App tasks" quantityTasks={this.state.tasks.length}/>

        <div className="container">
          <div className="row mt-4">

            <div className="col-md-4 text-center">
              <TasksForm onAddTask={this.addTask}></TasksForm>
            </div>

            <div className="col-md-8">
              <div className="row">
                {tasks}  
              </div>  
            </div>

          </div>
        </div>
      </div>
    );
  }
}

export default App;
